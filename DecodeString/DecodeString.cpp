#include <iostream>
#include <stack>

using namespace std;

string generateNumber(string exp) {
   string result;
   stack <int> st;
   
   for (int i = 0; i <= exp.length(); i++) {
       st.push(i + 1);
       
       if (i == exp.length() || exp[i] == 'I') {
           while (!st.empty()) {
               result += to_string(st.top());
               st.pop();
           }
       }
   }
   return result;
}

int main() {
    string expression;
    cin >> expression;
    cout << generateNumber(expression) << endl;
    return 0;
}