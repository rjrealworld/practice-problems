from Odo import Odometer
import pytest

o = Odometer(4)

@pytest.mark.parametrize('num, result', [
    (1234, True), 
    (6789, True), 
    (1020, False),
    (-123, True), 
    (-11, False), 
    (-451, False),
    (3, True), 
    (2341, False), 
    (4321, False),
    (999, False), 
    (123456789, True), 
    (-123456789, True), 
    (12.34, True)])
def test_ascending(num, result):
    assert Odometer.is_ascending(abs(num)) == result
    
@pytest.mark.parametrize('position, result', [
    (1, 1), 
    (2, 3), 
    (123, 0)])
def test_next_reading(position, result):
    o.next_reading(position)
    assert o.position == result

@pytest.mark.parametrize('position, result', [
    (1, 125), 
    (2, 123), 
    (126, 123)])
def test_prev_reading(position, result):
    o.prev_reading(position)
    assert o.position == result

def test_diff():
    odometer2 = Odometer(4)
    odometer2.next_reading(5)
    assert o.diff(odometer2) == 5

# def test_next_reading():
#     o.next_reading(2)
#     assert(o.position == 2)

#     # o.position = o.LENGTH - 1
#     o.next_reading(o.LENGTH - 2)
#     assert(o.position == 0)

#     o.next_reading(o.LENGTH)
#     assert(o.position == 0)

# def test_prev_reading():
#     o.prev_reading(2)
#     assert(o.position == 124)

#     # o.position = o.LENGTH - 1
#     o.prev_reading(o.LENGTH - 2)
#     assert(o.position == 0)

#     o.prev_reading(o.LENGTH)
#     assert(o.position == 0)