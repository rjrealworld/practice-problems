def is_valid_reading(reading: str) -> bool:
    reading = str(reading)
    if '0' in reading:
        return False
    return ''.join(sorted(set(reading))) == reading

def increament(reading: str) -> int:
    lowest_reading = (''.join([str(i) for i in range (1, len(reading)+1)]))
    highest_reading = (''.join([str(i) for i in range (9-len(reading)+1, 10)]))
    if reading == highest_reading:
        return int(lowest_reading)
    reading = int(reading)
    reading += 1
    while (not is_valid_reading(reading)):
        reading += 1
    return reading

def nth_next_reading(reading1: int, n: int):
    if not is_valid_reading(reading1):
        return 'Not valid reading'
    i = 1
    while i <= n:
        reading1 = increament(reading1)
        i += 1
    return reading1

def decrement(reading: str) -> int:
    lowest_reading=(''.join([str(i) for i in range (1, len(reading)+1)]))
    highest_reading=(''.join([str(i) for i in range (9-len(reading)+1, 10)]))
    if reading == lowest_reading :
        return int(highest_reading)
    reading = int(reading)
    reading -= 1
    while (not is_valid_reading(reading)) :
        reading -= 1
    return reading

def nth_prev_reading(reading1: int) :
    if not is_valid_reading(reading1):
        return 'Not valid reading'
    i = 1
    while i <= n :
        reading1 = decrement(reading1)
        i += 1
    return reading1

def count_readings(start, limit):
    count = 0
    while start != limit:
        start = increament(str(start))
        count += 1
    return count

def distance (reading1, reading2):
    if not is_valid_reading(reading1) or not is_valid_reading(reading2):
        return 'Not a valid reading'
    if reading1 == reading2:
        return 0
    lowest_reading = ''.join( [str(i) for i in range (1, len(str(reading1))+1)] )
    highest_reading = ''.join([str(i) for i in range (9-len(str(reading1))+1, 10)])
    if (reading1 == int(lowest_reading) and reading2 == int(highest_reading)) or (reading2 == int(lowest_reading) and reading1 == int(highest_reading)):
        return 1
    else:
        return count_readings(reading1, reading2) if reading1 < reading2 else count_readings(reading2, reading1)
