def findPeak(array: [int], length: int):
    peak = array[int(length / 2)]
    if length == 1:
        return peak
    if array[int(length / 2)] < array[int(length / 2) - 1]:
        return findPeak(array[:int(length / 2)], int(length / 2))
    elif array[int(length / 2)] < array[int(length / 2) + 1]:
        return findPeak(array[int(length / 2) + 1:], int(length / 2))
    else:
        return 'No peak'

arr = [1, 2, 3, 4, 5, 0, -1]
print(findPeak(arr, len(arr)))

#Explanation:
#Check the middle element, if it is less than left part then look only the left part i.e. 0 to n/2-1
#Similarly check if the element is less than the right part then look only the right part i.e. n/2+1 to n
#If both the conditions fail then return the element
#Also if the length of array is 1 then the element available will be the only peak

# Time complexity:
# For 1 element we check 0 times
# for 2 elements we check 1 time
# for 4 elements we check 2 times
# T(n) = O(log2 (n))