/* ----------------------------------------------------*
 * Represents the closed interval [a, b] where a, b    *
 * are integers.                                       *
 * ----------------------------------------------------*/
import java.util.*;
public class Range {

    protected int lo;
    protected int hi;

    public Range() {
        this.lo = this.hi = 0;
    }

    public Range(int e) {
        this.lo = 0;
        this.hi = e;
    }

    public Range(int a, int b) {
        this.lo = Math.min(a, b);
        this.hi = Math.max(a, b);
    }

    private void reset() {
        this.lo = this.hi = 0;
    }

    public void rStretch() {
        this.hi++;
    }

    public void rStretch(int n) {
        this.hi += n;
    }

    public void lStretch() {
        this.lo--;
    }

    public void lStretch(int n) {
        this.hi -= n;
    }

    public void stretch() {
        this.lo--;
        this.hi++;
    }

    public void stretch(int n) {
        this.lo -= n;
        this.hi += n;
    }

    public void squeeze() {
        this.lo++;
        this.hi--;
        if (this.lo > this.hi)
            this.reset();
    }

    public void squeeze(int n) {
        this.lo += n;
        this.hi -= n;
        if (this.lo > this.hi)
            this.reset();
    }

    public void shift(int n) {
        this.lo += n;
        this.hi += n;
    }
    
    public int length() {
        return this.hi - this.lo + 1;
    }
    
    public String toString() {
        return "[" + this.lo + ", " + this.hi + "]";
    }

    public boolean contains(int x) {
        return this.lo <= x && x <= this.hi;
    }
    public boolean contains(Range r) {
        return this.lo <= r.lo && r.hi <= this.hi;
    }
    public boolean equals(Range r) {
        return this.lo == r.lo && this.hi == r.hi;
    }
    public boolean isDisjoint(Range r) {
        return this.lo > r.hi || this.hi < r.lo;
    }
    public boolean isOverlapping(Range r) {
        return !(this.isDisjoint(r));
    } 
    public boolean lessThan(Range r) {
        return this.lo < r.lo;
    }
    public enum Relation {
        SUBSET, SUPERSET, OVERLAPL, OVERLAPR, TOUCHINGL, TOUCHINGR, LESSDISJOINT, MOREDISJOINT, SAME;
    }
    public Relation classify(Range r) {
        if (this.hi == r.lo) 
            return Relation.TOUCHINGR;
        if (this.lo == r.hi)
            return Relation.TOUCHINGL;
        if (this.equals(r))
            return Relation.SAME;
        if (this.contains(r)) 
            return Relation.SUPERSET;
        if (r.contains(this))
            return Relation.SUBSET;
        if (this.isDisjoint(r))
            if (this.lo > r.hi)
                return Relation.MOREDISJOINT;
            else
                return Relation.LESSDISJOINT;
        if (this.lessThan(r))
            return Relation.OVERLAPL;
        return Relation.OVERLAPR;
    }

    public Range merge(Range r) {
        if (this.isDisjoint(r)) {
            return new Range();
        }
        int a = Math.min(this.lo, r.lo);
        int b = Math.max(this.hi, r.hi);
        return new Range(a, b);
    }            

    public Range common(Range r) {
        if (this.isDisjoint(r)) {
            return new Range();
        }

        ArrayList<Integer> es = new ArrayList<Integer>();
        es.add(this.lo);
        es.add(this.hi);
        es.add(r.lo);
        es.add(r.hi);
        Collections.sort(es);
        return new Range(es.get(1), es.get(2));
	//return Range(sorted([self.lo, self.hi, r.lo, r.hi])[1:3])
    }
}
