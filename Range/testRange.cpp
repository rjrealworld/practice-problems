#include "range.cpp"
#include <gtest/gtest.h>

using namespace std;

struct containsTests : public testing::Test {
    range *rng;
    void SetUp() {
        rng = new range(2, 5);
    }
    void TearDown() {
        delete rng;
    }
};

// right stretch by 1
TEST_F(containsTests, rStretchTestby1) {
    rng->rStretch();
    ASSERT_EQ(rng->getHi(), 6);
}
// right stretch by N
TEST_F(containsTests, rStretchTestbyN) {
    rng->rStretch(3);
    ASSERT_EQ(rng->getHi(), 8);
}

// left stretch by 1
TEST_F(containsTests, lStretchTestby1) {
    rng->lStretch();
    ASSERT_EQ(rng->getLow(), 1);
}
// left stretch by N
TEST_F(containsTests, lStretchTestbyN) {
    rng->lStretch(2);
    ASSERT_EQ(rng->getLow(), 0);
}

// stretch by 1
TEST_F(containsTests, StretchTestby1) {
    rng->stretch();
    ASSERT_EQ(rng->getLow(), 1);
    ASSERT_EQ(rng->getHi(), 6);
}
// stretch by N
TEST_F(containsTests, StretchTestbyN) {
    rng->stretch(2);
    ASSERT_EQ(rng->getLow(), 0);
    ASSERT_EQ(rng->getHi(), 7);
}

// squeeze
TEST_F(containsTests, squeeze) {
    rng->squeeze();
    ASSERT_EQ(rng->getLow(), 3);
    ASSERT_EQ(rng->getHi(), 4);
}
// squeeze by N
TEST_F(containsTests, squeezeByN) {
    rng->squeeze(2);
    ASSERT_EQ(rng->getLow(), 0);
    ASSERT_EQ(rng->getHi(), 0);
}

// shift by N
TEST_F(containsTests, shiftbyN) {
    rng->shift(2);
    ASSERT_EQ(rng->getLow(), 4);
    ASSERT_EQ(rng->getHi(), 7);
}

// length
TEST_F(containsTests, length) {
    int len = rng->length();
    ASSERT_EQ(len, 4);
}

// to string
TEST_F(containsTests, toStr) {
    string str = rng->toString();
    ASSERT_EQ(str, "[2, 5]");
}

// contains integer
TEST_F(containsTests, containsEvenNumber) {
    bool contains = rng->contains(2);
    ASSERT_TRUE(contains);
}
TEST_F(containsTests, containsOddNumber) {
    bool contains = rng->contains(3);
    ASSERT_TRUE(contains);
}
// contains Range
TEST_F(containsTests, isSubs) {
    range rng_2(3, 5);
    bool isSubRange = rng->contains(rng_2);
    ASSERT_TRUE(isSubRange);
}

// equal ranges
TEST_F(containsTests, areEqual) {
    range rng_2(2, 5);
    bool areEq = rng->equals(rng_2);
    ASSERT_TRUE(areEq);
}

// disjoint
TEST_F(containsTests, disjiont) {
    range rng_2(6, 9);
    bool disjoint = rng->isDisjoint(rng_2);
    ASSERT_TRUE(disjoint);
}

// overlaps
TEST_F(containsTests, overlaps) {
    range rng_2(5, 9);
    bool overlaps = rng->isOverlapping(rng_2);
    ASSERT_TRUE(overlaps);
}

// merge disjoints
TEST_F(containsTests, mergeDisjoint) {
    range rng_2(6, 9);
    range merged = rng->merge(rng_2);
    ASSERT_EQ(merged.getLow(), 0);
    ASSERT_EQ(merged.getHi(), 0);
}
// merge
TEST_F(containsTests, merge) {
    range rng_2(4, 9);
    range merged = rng->merge(rng_2);
    ASSERT_EQ(merged.getLow(), 2);
    ASSERT_EQ(merged.getHi(), 9);
}

// common
TEST_F(containsTests, commonInRange) {
    range rng_2(4, 9);
    range com = rng->common(rng_2);
    ASSERT_EQ(com.getLow(), 4);
    ASSERT_EQ(com.getHi(), 5);
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}







// TEST_F(containsTests, overlaps) {
//     range rng_2(4, 9);
//     bool doesOverlap = rng->isOverlaping(rng_2);
//     ASSERT_TRUE(doesOverlap);
// }

// TEST_F(containsTests, notOverlaps) {
//     range rng_2(6, 9);
//     bool doesOverlap = rng->isOverlaping(rng_2);
//     ASSERT_FALSE(doesOverlap);
// }

// TEST_F(containsTests, isLessThan) {
//     range rng_2(4, 9);
//     bool lessThan = rng->isLessThan(rng_2);
//     ASSERT_TRUE(lessThan);
// }

// TEST_F(containsTests, isMoreThan) {
//     range rng_2(4, 9);
//     bool moreThan = rng->isMoreThan(rng_2);
//     ASSERT_TRUE(moreThan);
// }

// TEST_F(containsTests, isSubs) {
//     range rng_2(4, 9);
//     bool isSubset = rng->isSubRange(rng_2);
//     ASSERT_TRUE(isSubset);
// }

// TEST_F(containsTests, doTouch) {
//     range rng_2(4, 9);
//     bool isTouching = rng->areTouching(rng_2);
//     ASSERT_TRUE(isTouching);
// }