#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Range {
    protected:
        int a, b;

        void reset() {
            a = 0;
            b = 0;
        }
    
    public:
        //Constructors
        Range() {
            a = 0;
            b = 0;
        }
        Range (int limit) { 
            a = 0; 
            b = limit;
        }
        Range(int start, int limit) {
            a = start;
            b = limit; 
        }

        //length of range
        int length() {
            return b - a + 1;
        }
        
        //comparison
        int equals(Range r) {
            return (a == r.a && b == r.b);
        }
        bool lessThan (Range r) { 
            return (a < r.a || length() < r.length());
        }
        bool greaterThan(Range r) {
            return (!equals(r) && !lessThan(r));
        }

        //rangeA contains rangeB/val
        bool contains(Range r) {
            return (r.a >= a && r.b <= b);
        }
        bool contains(int val) {
            return (val >= a && val <= b); 
        }

        bool disjoint (Range r) {
            bool isStartBetween = r.a >= a && r.a <= b;
            bool isEndBetween = r.b >= a && r.b <= b;
            return (!isStartBetween) && (!isEndBetween);
        }
        bool overlap (Range r) {
            return (!(disjoint(r)));
        }
        bool touching (Range r) {
            return ((r.a == b) || (a == r.b));
        }

        bool isSubset(Range r) {
            return a >= r.a && b <= r.b;
        }
        bool isSuperset(Range r) {
            return r.a >= a && r.b <= b;
        }

        Range merge (Range r) {
            if (disjoint(r))
                return Range();
            return Range(min(a, r.a), max(b, r.b));
        }

        Range common(Range r) {
            if (disjoint(r)) {
                return Range();
            }

            vector < int > elements;
            elements.push_back(a);
            elements.push_back(b);
            elements.push_back(r.a);
            elements.push_back(r.b);
            sort(elements.begin(), elements.end());
            return Range(elements[0], elements[1]);
        }

        //stretch
        void rStretch(int n = 1) {
            b += n;
        }
        void lStretch(int n = 1) {
            a -= n;
        }
        void stretch(int n = 1) {
            a -= n;
            b += n;
        }

        //squeeze
        void squeeze(int n = 1) {
            a += n;
            b -= n;
            if (a > b)
                reset();
        }

        //shift
        void lShift(int n = 1) {
            a -= n;
            b -= n;
        }
        void rShift(int n = 1) {
            a += n;
            b += n;
        }

        vector < int > elements() {
            vector<int> allElements;
            for (int element = a; element <= b; element++) 
                allElements.push_back(element);
            return allElements;
        }

        enum Relation {
            SUBSET, SUPERSET, OVERLAPLEFT, OVERLAPRIGHT, TOUCHINGLEFT, TOUCHINGRIGHT, LEFTDISJOINT, RIGHTDISJOINT, SAME
        };
        Relation classify(Range r) {
            if (b == r.a) 
                return TOUCHINGRIGHT;
            if (a == r.b)
                return TOUCHINGLEFT;
            if (equals(r))
                return SAME;
            if (contains(r)) 
                return SUPERSET;
            if (r.contains(*this))
                return SUBSET;
            if (disjoint(r))
                if (a > r.b)
                    return RIGHTDISJOINT;
                else
                    return LEFTDISJOINT;
            if (lessThan(r))
                return OVERLAPLEFT;
            return OVERLAPRIGHT;
        }
};


/* ----------------------------------------------------*
/* Represents the closed interval [a, b]               *
/* where a, b are integers.                            *
/* ----------------------------------------------------*/

//ClosedRange() = Range(0, 0) = [0], 
//ClosedRange(0) = Range(0, 0) = [0]
//ClosedRange(0, 1) = Range(0, 1) = [0,1]
//ClosedRange(1) = Range(0, 1) = [0, 1]
//ClosedRange(1, 2) = Range(1, 2) = [1,2]

class ClosedRange : public Range {
    public: 
        ClosedRange(){
            a = 0;
            b = 0;
        }
        ClosedRange(int limit){
            a = 0;
            b = limit;
        }
        ClosedRange(int start, int limit){
            a = start;
            b = limit;
        }
        string toString() {
            return  ("[" + to_string(a) + ", " + to_string(b) + "]");
        }
};


/* ----------------------------------------------------*
/* Represents the open  interval (a, b)                *
/* where a, b are integers.                            *
/* ----------------------------------------------------*/

//OpenRange() = Range() = (0, 0), 
//OpenRange(0) = Range(0, 0) = (0, 0)
//OpenRange(1) = Range(1, 1) = (0, 1)
//OpenRange(2) = Range(1, 2) = (0, 2)
//OpenRange(0, 1) = Range(1, 1) = (0, 1)
//OpenRange(0, 2) = Range(1, 2) = (0, 2)
//OpenRange(1, 2) = Range(2, 2) = (1, 2)

class OpenRange : public Range {
     public:
        OpenRange() {
            a = 0;
            b = 0;
        }
        OpenRange(int limit) {
            a = 0;
            b = limit - 1;
        }
        OpenRange(int start, int limit) {
            a = start + 1;
            b = limit - 1;
        }
        string toString() {
            return "(" + to_string(a - 1) + ", " + to_string(b + 1) + ")";
        }
};


/* ----------------------------------------------------*
/* Represents the open  interval (a, b]                *
/* where a, b are integers.                            *
/* ----------------------------------------------------*/

class SemiOpenLeft: public Range {
    public:
        SemiOpenLeft() {
            a = 0;
            b = 0;
        }
        SemiOpenLeft(int limit) {
            a = 0;
            b = limit;
        }
        SemiOpenLeft(int start, int limit) {
            a = start + 1;
            b = limit;
        }
        string toString() {
            return "(" + to_string(a - 1) + ", " + to_string(b) + "]";
        }
};


/* ----------------------------------------------------*
/* Represents the open  interval [a, b)                *
/* where a, b are integers.                            *
/* ----------------------------------------------------*/

class SemiOpenRight: public Range {
    public:
        SemiOpenRight() {
            a = 0;
            b = 0;
        }
        SemiOpenRight(int limit) {
            a = 0;
            b = limit - 1;
        }
        SemiOpenRight(int start, int limit) {
            a = start;
            b = limit - 1;
        }
        string toString() {
            return "[" + to_string(a) + ", " + to_string(b + 1) + ")";
        }
};

int main() {
    Range r1(10, 20);
    Range r2(3, 30);
    ClosedRange r3(1, 5);
}