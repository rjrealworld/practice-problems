For calculating the water trap, iterate through the height array and compare it with the heighest building possible in its left and right. 
The water stored aboove ith building is the difference between the ith building height and the minimum of its left/right neighbour building. 
For example for the 2nd index i.e. of height zero, the max of left side is 1 and the max of right side is 3 but the water that can be stored is 1 - 0 = 1 units only. Similarly proceed further.
