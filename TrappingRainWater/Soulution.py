class Solution:
    def trap(self, height: List[int]) -> int:
        water = 0
        for index in range (0, len(height)):
            left = max(height[: index + 1])
            right = max(height[index :])
            current = height[index]
            if current < left and current < right:
                water += min(left - current, right - current)
        return water