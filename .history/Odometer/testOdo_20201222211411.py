from Odo import Odometer
import pytest

o = Odometer(4)

@pytest.mark.parametrize('num, result', [
    (1234, True), 
    (6789, True), 
    (1020, False),
    (-123, True), 
    (-11, False), 
    (-451, False),
    (3, True), 
    (2341, False), 
    (4321, False),
    (999, False), 
    (123456789, True), 
    (-123456789, True), 
    (12.34, True)])
def test_ascending(num, result):
    assert Odometer.is_ascending(abs(num)) == result
    
@pytest.mark.parametrize('position, result', [(1, 1), (2, 3), (5, 2)])
def test_next_reading(position, result):
    odometer1.next_reading(position)
    assert odometer1.position == result

@pytest.mark.parametrize('position, result', [(1, 1), (2, 5), (5, 0)])
def test_prev_reading(position, result):
    odometer1.prev_reading(position)
    assert odometer1.position == result

def test_diff():
    odometer2 = Odometer(4)
    odometer2.next_reading(5)
    assert odometer1.diff(odometer2) == 5

def test_next_reading():
    o.next_reading(2)
    assert(o.position == 2)

    # o.position = o.LENGTH - 1
    o.next_reading(o.LENGTH - 2)
    assert(o.position == 0)

    o.next_reading(o.LENGTH)
    assert(o.position == 0)

def test_prev_reading():
    o.prev_reading(2)
    assert(o.position == 124)

    # o.position = o.LENGTH - 1
    o.prev_reading(o.LENGTH - 2)
    assert(o.position == 0)

    o.prev_reading(o.LENGTH)
    assert(o.position == 0)