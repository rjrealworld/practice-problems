from Odo import Odometer
import pytest

o = Odometer(4)

@pytest.mark.parametrize('num, result', [(1234, True), (6789, True), (1020, False), 
                                        (-123, True), (-11, True), (-451, False), 
                                        (3, True), (2341, False), (4321, False), 
                                        (999, True), (123456789, True), (-123456789, True), (12.34, True)])
def test_ascending(num, result):
    assert o.is_ascending(num) == result

# def test_is_ascending():
#     assert(o.is_ascending(3) == True)
#     assert(o.is_ascending(1234) == True)
#     assert(o.is_ascending(4231) == False)
#     assert(o.is_ascending(5678) == True)
#     assert(o.is_ascending(1020) == False)
#     assert(o.is_ascending(9999) == False)
#     assert(o.is_ascending(-123) == True)
#     assert(o.is_ascending(-451) == False)
#     assert(o.is_ascending(123456789) == True)
#     assert(o.is_ascending(-123456789) == True)
    

def test_next_reading():
    o.next_reading(2)
    assert(o.position == 2)

    # o.position = o.LENGTH - 1
    o.next_reading(o.LENGTH - 2)
    assert(o.position == 0)

    o.next_reading(o.LENGTH)
    assert(o.position == 0)

def test_prev_reading():
    o.prev_reading(2)
    assert(o.position == 124)

    # o.position = o.LENGTH - 1
    o.prev_reading(o.LENGTH - 2)
    assert(o.position == 0)

    o.prev_reading(o.LENGTH)
    assert(o.position == 0)