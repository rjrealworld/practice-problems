from Odo import Odometer
import pytest

o = Odometer(4)

@pytest.mark.parametrize('num, result', [(1234, True), (6789, True), (1020, False), 
                                        (-123, True), (-11, False), (-451, False), 
                                        (3, True), (2341, False), (4321, False), 
                                        (999, True), (123456789, True), (-123456789, True), (12.34, True)])
def test_ascending(num, result):
    assert Odometer.is_ascending(abs(num)) == result
    

def test_next_reading():
    o.next_reading(2)
    assert(o.position == 2)

    # o.position = o.LENGTH - 1
    o.next_reading(o.LENGTH - 2)
    assert(o.position == 0)

    o.next_reading(o.LENGTH)
    assert(o.position == 0)

def test_prev_reading():
    o.prev_reading(2)
    assert(o.position == 124)

    # o.position = o.LENGTH - 1
    o.prev_reading(o.LENGTH - 2)
    assert(o.position == 0)

    o.prev_reading(o.LENGTH)
    assert(o.position == 0)